%Estados iniciales

estado(e0,[],[b,m,m,m,c,c,c]).
estado(e1,[c],[b,m,m,m,c,c]).
estado(e2,[m,c],[b,m,m,c,c]).
estado(e3,[c,c],[b,m,m,m,c]).
estado(e4,[m,m,m],[b,c,c,c]).
estado(e5,[c,c,c],[b,m,m,m]).
estado(e6,[m,m,c,c],[b,m,c]).
estado(e7,[m,m,m,c],[b,c,c]).
estado(e8,[m,m,m,c,c],[b,c]).
estado(e9,[b,c],[m,m,m,c,c]).
estado(e10,[b,m,c],[m,m,c,c]).
estado(e11,[b,c,c],[m,m,m,c]).
estado(e12,[b,m,m.m],[c,c,c]).
estado(e13,[b,c,c,c],[m,m,m]).
estado(e14,[b,m,m,c,c],[m,c]).
estado(e15,[b,m,m,m.c],[c,c]).
estado(e16,[b,m,m,m,c,c],[c]).
estado(e17,[b,m,m,m,c,c,c],[]).

%movientos posibles
movim(m1,[m,m]).
movim(m2,[m]).
movim(m3,[m,c]).
movim(m4,[c,c]).
movim(m5,[c]).

%movimientos
mover(e0,m3,e10).
mover(e0,m4,e11).
mover(e0,m5,e9).

mover(e1,m2,e10).
mover(e1,m4,e13).
mover(e1,m5,e11).

mover(e2,m1,e15).
mover(e2,m3,e14).

mover(e3,m1,e14).
mover(e3,m5,e13).

mover(e4,m4,e16).
mover(e4,m5,e15).


%no hay movimientos en el estado e5
mover(e6,m2,e16).
mover(e6,m3,e17).

mover(e7,m4,e17).
mover(e7,m5,e16).

mover(e8,m5,e17).
mover(e9,m5,e0).

mover(e10,m2,e1).
mover(e10,m3,e0).

mover(e11,m4,e0).
mover(e11,m5,e1).

%no hay movimientos en el estado 12

mover(e13,m4,e1).
mover(e13,m5,e3).

mover(e14,m1,e3).
mover(e14,m3,e2).

mover(e15,m1,e2).
mover(e15,m5,e4).

mover(e15,m1,e2).
mover(e15,m5,e4).

mover(e16,m2,e6).
mover(e16,m4,e4).
mover(e16,m5,e7).

mover(e17,m3,e6).
mover(e17,m4,e7).
mover(e17,m5,e8).

inicial(e0).
final(e17).

%%búsqueda

solucion(S) :- inicial(E),
                prolongable([],[E],LM,LE),
                reverse(LM,S),
                nl,nl,imprimir(S).

prolongable(LM,[E|ER], LM, [E|RE]) :- final(E).

prolongable(LM,[E|ER],LMP,LEP):-
  mover(E,M,NE),
  not(member(NE,[E|ER])),
  prolongable([M|LM],[NE,E|ER],LMP,LEP).


imprimir([]) :- nl.
imprimir([M|Resto]) :- escribe(M), nl, imprimir(Resto).

escribe(m1):- write ('Cruzan 2 misioneros').
escribe(m2):- write ('Cruzan 1 misionero').
escribe(m3):- write ('Cruzan 1 misionero y un caníbal').
escribe(m4):- write ('Cruzan 2 caníbales').
escribe(m5):- write ('Cruza 1 caníbal').
